ix_help() {
read -r -d '' MSG <<-'EOF'

Interact with ix.io pastebin.

     ix [-d ID] [-i ID] [-n N] [opts]

     ID   - ID of post to delete or replace
     N    - number of times post can be accessed before self-destruct
     opts - basic-auth info, if needed, e.g.: '-u MaDeuce:KennyE' 
            If pastebin created with basic-auth, then basic-auth needed
            to read/replace/remove.

Examples:

     ix hello.txt              # paste file (name/ext will be set).
     echo Hello world. | ix    # read from STDIN (won't set name/ext).
     ix -n 1 self_destruct.txt # paste will be deleted after one read.
     ix -i ID hello.txt        # replace ID, if you have permission.
     ix -d ID

ix.io hints:

    Anonymous, unnamed paste, two ways:

        cat file.ext | curl -F 'f:1=<-' ix.io
        curl -F 'f:1=@file.ext' ix.io

    Delete ID, two ways:

        curl -n -X DELETE ix.io/ID
        curl -F 'rm=ID' USER:PASS@ix.io

    Replace ID, two ways:

        curl -n -X PUT -F 'f:1=@file.ext' ix.io/ID
        cat file.ext | curl -F 'f:1=<-' -F 'id:1=ID' USER:PASS@ix.io

    Paste that can be only be read twice:

        cat file.ext | curl -F 'f:1=<-' -F 'read:1=2' ix.io

    List last 100 anonymous posts:

        http://ix.io/user/

    List posts from user:

        http://ix.io/user/MaDeuce

EOF
echo "$MSG"
return
}

ix() {
    local opts
    local OPTIND
    [ -f "$HOME/.netrc" ] && opts='-n'
    while getopts ":hd:i:n:" x; do
        case $x in
            h) ix_help; return;;
            d) $echo curl $opts -X DELETE ix.io/$OPTARG; return;;
            i) opts="$opts -X PUT"; local id="$OPTARG";;
            n) opts="$opts -F read:1=$OPTARG";;
        esac
    done
    shift $(($OPTIND - 1))
    [ -t 0 ] && {
        local filename="$1"
        shift
        [ "$filename" ] && {
            curl $opts -F f:1=@"$filename" $* ix.io/$id
            return
        }
        echo "^C to cancel, ^D to send."
    }
    curl $opts -F f:1='<-' $* ix.io/$id
}

