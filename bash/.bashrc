#  IF YOU WANT SOMETHING TO BE AVAILABLE FOR BOTH LOGIN
#  AND NON-LOGIN INTERACTIVE SHELLS, PUT IT IN HERE.
#
# Bash initialization cheat sheet.  Read down the appropriate
# column. Executes A, then B, then C, etc. The B1, B2, B3 means it
# executes only the first of those files found.
#
# +----------------+-----------+-----------+------+
# |                |Interactive|Interactive|Script|
# |                |login      |non-login  |      |
# +----------------+-----------+-----------+------+
# |/etc/profile    |   A       |           |      |
# +----------------+-----------+-----------+------+
# |/etc/bash.bashrc|           |    A      |      |
# +----------------+-----------+-----------+------+
# |~/.bashrc       |           |    B      |      |
# +----------------+-----------+-----------+------+
# |~/.bash_profile |   B1      |           |      |
# +----------------+-----------+-----------+------+
# |~/.bash_login   |   B2      |           |      |
# +----------------+-----------+-----------+------+
# |~/.profile      |   B3      |           |      |
# +----------------+-----------+-----------+------+
# |BASH_ENV        |           |           |  A   |
# +----------------+-----------+-----------+------+
# |                |           |           |      |
# +----------------+-----------+-----------+------+
# |                |           |           |      |
# +----------------+-----------+-----------+------+
# |~/.bash_logout  |    C      |           |      |
# +----------------+-----------+-----------+------+
#
# By default bash reads .bashrc *only* for interactive non-login 
# shells. Login shells read *only* .bash_profile.  Since I
# typically wish to share much between both login and non-login
# interactive shells, *my* .bash_profile sources in .bashrc.
# So, the net effect is that *my* .bashrc is read by both login
# and non-login (i.e., *all* interactive) shells.
#

function configure_bash() {

    # At end of session, append history to histfile, rather than replace.
    shopt -s histappend

    export HISTSIZE=5000
    export HISTFILESIZE=5000
    export HISTTIMEFORMAT='%F %T '

    # Ignore duplicate commands
    export HISTCONTROL=ignoredups

    # Don't place matching commands in history
    export HISTIGNORE='ls:bg:fg:history'

    # Save to histfile for each command, rather than save all at end of session
    export PROMPT_COMMAND='history -a'

    # 'r' will rerun command matching arg.  E.g., 'r cc', reruns last 'cc'.
    alias r='fc -s'

    # TODO - probably don't want this in non-login shell!
    # EOF (^d) won't cause shell to exit
    export IGNOREEOF=10
    
    alias ls='ls -CF'
}

configure_bash


################################################################################
#
# A few basic tools
#
################################################################################

source_if_present () {
    [[ -f "$1" ]] && source "$1"
}

pathappend() {
    # Add $1 to the end of $PATH, provided that the directory
    # exists and that $1 is not already in $PATH.
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
        PATH="${PATH:+"$PATH:"}$1"
    fi
}

pathprepend() {
    # Add $1 to the beginning of $PATH, provided that the directory
    # exists and that $1 is not already in $PATH.
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
        PATH="${1}${PATH:+":$PATH"}"
    fi
}

################################################################################
################################################################################
#
# $PATH strategy:
#
# 1) Want my local ~/bin to be first in the path so that it is easy to override
# behavior by putting stuff there.  This can be any type of script or binary.
#
# 2) Then want the virtualenv currently in effect to be next in
# precedence.  This is so that it can override anything done by
# MacPorts of by the default system configuration of OS-X.  This only
# affects python behavior -- there should not be any scripts or
# binaries in the virtualenv path that are not python related.
# 
# 3) Next in precedence is the path to MacPorts.  This can be any sort
# of script or binary.  These can either be: a) things that don't come
# with OS-X that I have added via MacPorts, or b) things I've added
# via MacPorts that are newer/ different from a similar thing included
# with OS-X.  Either way, these items should be earlier in the path
# than the default OS-X stuff.
# 
# 4) The default $PATH as set by OS-X.
#
################################################################################
################################################################################

function configure_macports() {
    pathprepend /opt/local/bin
    pathprepend /opt/local/sbin
}

configure_macports

# N.B.: Do this *after* configure_macports 
# or anything else that modifies the front of PATH.
pathprepend ~/bin


################################################################################
#
# Remove Emacs Turds
#
################################################################################
function ret() {
    # Remove Emacs Turds -- Remove any file whose name ends with a '~'.
    for file in ./*~
    do
	[[ -e "$file" ]] && rm "$file"
    done
}

# special commands to interact with finder on OS-X
[[ "Darwin" == "`uname -s`" ]] && [[ -r ~/.bashrc_osx ]] && . ~/.bashrc_osx

# This function shortens a path so that it is no longer than a specified length.
# Intended to trim paths that will be included in PS1
_PS1 ()
{
    local PRE= NAME="$1" LENGTH="$2";
    [[ "$NAME" != "${NAME#$HOME/}" || -z "${NAME#$HOME}" ]] &&
        PRE+='~' NAME="${NAME#$HOME}" LENGTH=$[LENGTH-1];
    ((${#NAME}>$LENGTH)) && NAME="/...${NAME:$[${#NAME}-LENGTH+4]}";
    echo "$PRE$NAME"
}

# This will probably be updated/changed somewhere else, but until then...
# Will show $PWD to a max len of 20 characters.
PS1='\u@\h:$(_PS1 "$PWD" 20)\$ '
# export PS1='[\u@\H]\w> '

# reset xterm window to 24x80
# alias rs='resize -s 24 80'
# I have no idea what happened to /usr/X11/bin/resize...
alias rs='printf "\e[8;24;80t"'

alias cls=clear

# Setup all the packages provided by 'dotfiles'
source ~/.bash.d/dotfiles_init
