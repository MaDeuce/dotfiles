# Variables for the Phaxio fax sending service.  They are needed by 
# ~/bin/sendfax.  Look in that file for more info.

export PHAXIO_API_KEY="$DOTFILES_UNSET"
export PHAXIO_API_SECRET="$DOTFILES_UNSET"
