# Configure bash for Git

# Setup prompt with Git info
source_if_present ~/.bash.d/git-prompt.sh
source_if_present ~/.bash.d/git-completion.bash
export GIT_PS1_SHOWDIRTYSTATE=1
# example from git-prompt.sh
# PS1='[\u@\h \W$(__git_ps1 "( %s)")]\$ '
PS1='$(__git_ps1 "(%s)")$(_PS1 "$PWD" 20) \$ '
