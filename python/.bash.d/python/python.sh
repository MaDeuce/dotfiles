# If you see errors like:
#
#      UnicodeEncodeError: 'ascii' codec can't encode character u'\u2019' 
#                          in position 3: ordinal not in range(128)
# then you need the next line...
export PYTHONIOENCODING=utf_8

# command history
export PYTHONSTARTUP=~/.pystartup

function configure_virtualenv() {

    export WORKON_HOME=~/.virtualenvs
    export VIRTUAL_ENV_DISABLE_PROMPT=""
    export VIRTUALENVWRAPPER_VIRTUALENV_ARGS='--no-site-packages'

    # MMP = Major Minor Point (Version)
    # MM  = Major Minor       (Version)
    PYTHON_VER=$(python -V)             # "Python X.Y.Z"
    PYTHON_MMP=${PYTHON_VER/Python /}   # "X.Y.Z"
    PYTHON_MM=${PYTHON_MMP%.*}          # "X.Y"

    # virtualenvwrapper_lazy.sh calls virtualenvwrapper.sh.
    # Because of the way macports deploys the various versions of each,
    # virtualenvwrapper_lazy.sh-X.Y won't find virtualenvwrapper.sh.
    # To work around this, VIRTUALENVWRAPPER_SCRIPT is set to
    # the correct location.
    export VIRTUALENVWRAPPER_SCRIPT="/opt/local/bin/virtualenvwrapper.sh-${PYTHON_MM}"
    source  "/opt/local/bin/virtualenvwrapper_lazy.sh-${PYTHON_MM}"

}

configure_virtualenv
