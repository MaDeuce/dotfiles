# About ~/dotfiles/.bash.d/.bash.d

~~~~ {#mycode .text .numberLines startFrom="100"}

     ~/dotfiles/.bash.d/.bash.d/files...
          |        |      |	  |	   			
          |        |      |    	  \--- files to install ("FILES")
          |        |      |
	  |	   |	  \--- directory under ~/ in which to install FILES
	  |	   |
	  |	   \--- the name of this dotfiles package  
	  |												   
	  \--- directory containing the 'dotfiles' packages
~~~~

This is a part of the `.bash.d` package which is managed by the `dotfiles`
package manager.  This package is located in the `~/dotfiles/.bash.d/.bash.d`
directory.  This directory is intentionally kept empty, except for this README
file.  If you have the urge to check files into this directory, you probably
don't want to do so.

The path to this file is `~/dotfiles/bash.d/.bash.d/Readme.md`.  When the
`.bash.d` package is installed (via `make install`), the link
`.bash.d/README.md` is created which points to 
`~/dotfiles/bash.d/.bash.d/Readme.md`.

This directory, and this file, serve no technical purpose.  Package
installation will work properly is the `.bash.d` package is removed.   This
package and this file exist solely for documentation purposes.

Other packages will create additional links in `~/.bash.d` when they are
installed.  Each of these links will point to a file located somewhere in
`~/dotfiles`.  Each of these files will modify the user's environment when
they are sourced in via `dotfiles_init`.


