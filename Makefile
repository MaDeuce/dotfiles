# N.B. - stow must be run from ~/dotfiles
#        similar thing with git commands

# Relevant reading:
# http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html
# http://taihen.org/managing-dotfiles-with-gnu-stow/
# also a couple of good posts on news.ycombinator.com
#
# vcsh & mr
# https://github.com/RichiH/vcsh
# http://www.linuxjournal.com/content/manage-your-configs-vcsh
# This is a viable alternative, but it seems needlessly complex.

# Q: Why can stow be run with no other args?
# A: The stow directory is assumed to be the value of the "STOW_DIR"
# environment variable or if unset the current directory, and the target
# directory is assumed to be the parent of the current directory (so it
# is typical to execute stow from the directory /usr/local/stow). Each
# package given on the command line is the name of a package in the stow
# directory (e.g., perl). By default, they are installed into the target
# directory (but they can be deleted instead using "-D").  


PACKAGES  = __private__ bash emacs git ix phaxio bashmarks python bin dotfiles
DOCS = Readme.md documentation.pdf

.PHONY: $(PACKAGES)


######################################################################
# Installation

install: $(PACKAGES)

$(PACKAGES):
	stow $@

uninstall_all: 
	stow --delete $(PACKAGES)


######################################################################
# Documentation

docs: $(DOCS)

%.pdf: %.md 
	pandoc $< \
        -V dfversion=1.0.0 \
	-f markdown+fenced_code_blocks+fenced_code_attributes -s -o $@

# Bitbucket can't display pandoc markdown.
# Create a dumbed-down markdown for them, which they auto display.
Readme.md: documentation.md
	pandoc documentation.md \
	       -f markdown+fenced_code_blocks+fenced_code_attributes \
               -s -t markdown_strict > Readme.md

# This file is handy when editing directories shown in documentation.md
tree_dotfiles.txt:
	@tree --noreport --charset ascii -a \
              -I '*.DS_Store*|*.git*|*.pdf|*~|.#*|#*#' ~/dotfiles \
           | sed 's/`/\\/g' > tree_dotfiles.txt


######################################################################
# Miscellaneous

tidy:
	@find . -name \*~ -exec rm {} \;
	@rm -f tree_dotfiles.txt

clean: tidy
	@rm -f *pdf
	@rm -f Readme.md

