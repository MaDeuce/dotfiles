;;;
;;; os-x.el
;;;

;;; Early stages of an OS-X specific .el
;;; Not currently used.


(defun mac-open-terminal ()
  ;;
  ;; open a terminal window
  ;;
  (interactive)
  (let ((dir ""))
    (cond
     ((and (local-variable-p 'dired-directory) dired-directory)
      (setq dir dired-directory))
     ((stringp (buffer-file-name))
      (setq dir (file-name-directory (buffer-file-name))))
     ((stringp default-directory)
      (setq dir default-directory))
     )
    (do-applescript
     (format "
 tell application \"Terminal\"
   activate
   try
     do script with command \"cd %s\"
   on error
     beep
   end try
 end tell" dir))
    ))


(defun new-emacs ()
  ;;
  ;; Start a new instance of emacs
  ;;
  (interactive)
  (shell-command "open -n -a /Applications/MacPorts/Emacs.app"))
