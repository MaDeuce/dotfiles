;;; Disable the incredibly annoying splash screen
(setq inhibit-startup-message 1)

;;; Tell emacs to save customizations, if any to a file
;;; other than ~/.emacs (the default).  This makes it easy to try out
;;; customizations without making 'dotfiles' think something important
;;; has changed.
(setq custom-file "~/.emacs-custom.el")
(load custom-file)

;;; Enable three button mouse on OS-X
;;;
;;;    button 2 = <option> click
;;;    button 3 = <command> click
;;;
(setq mac-emulate-three-button-mouse t)

;;; Mac ports installs executable here.  Not sure why it isn't
;;; in the path that emacs is using...
(add-to-list 'exec-path "/opt/local/bin/")

;;; Personal stash of elisp
(add-to-list 'load-path "~/.emacs.d/lisp/")

(when (>= emacs-major-version 24)
  ;; First load package.el, which is required by use-package
  ;; One can manually manage packages via 'M-x list-packages'
  (require 'package)
  (add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/") t)
  ;; (packate-refresh-contents)  ; can take a while, not mandatory
  (package-initialize)

  ;; If use-package is not installed, use package.el to get it.
  (if (not (package-installed-p 'use-package))
      (progn
	(package-refresh-contents)
	(package-install 'use-package)))

  ;; Now that we know use-package is installed, we'll load it up.
  ;; But it only needs to be loaded when this file is compiled.
  (eval-when-compile
    (require 'use-package))

  ;; From this point on, use 'use-package' instead of 'require'
  )

;;; Enable upcase and downcase region
(put 'downcase-region 'disabled nil)
(put 'upcase-region   'disabled nil)

;;; Display column and line numbers in all buffers
(global-linum-mode 1)
(setq line-number-mode t)
(setq column-number-mode t)
;;;(use-package smooth-scrolling :ensure smooth-scrolling)
(with-eval-after-load "linum"
  ;; set `linum-delay' so that linum uses
  ;; `linum-schedule' to update linums.
  (setq linum-delay t)

  ;; create a new var to keep track of 
  ;; the current update timer.
  (defvar-local my-linum-current-timer nil)

  ;; rewrite linum-schedule so it waits for
  ;; 1 second of idle time before updating,
  ;; and so it only keeps one active idle
  ;; timer going
  (defun linum-schedule ()
    (when (timerp my-linum-current-timer)
      (cancel-timer my-linum-current-timer))
    (setq my-linum-current-timer
	  (run-with-idle-timer 
	   1 
	   nil #'linum-update-current))))


;;; Make all 'yes or no' prompts show 'y or n'
(fset 'yes-or-no-p 'y-or-n-p)

;;; Disable the damn bell.  Primarily bothers me when scrolling
;;; at the end (or beginning) of a buffer.
(setq ring-bell-function 'ignore)

;;;(global-hl-line-mode)   ; highlight current line, all buffers

(defun os-x-setup ()
  ;; alternative: 1 line at a time, progressive speed t, but scrolling buffers
  (setq mouse-wheel-scroll-amount '(5 ((shift) . 1)))  ; five lines at a time
  (setq mouse-wheel-progressive-speed nil)             ; don't accel scrolling
)

(defun os-x-setup2 ()
  (setq mouse-wheel-scroll-amount '(0.025))
  (setq mouse-wheel-progressive-speed nil)
)

(os-x-setup2)

(defun org-mode-setup ()
  (require 'org)
  (define-key global-map "\C-cl" 'org-store-link)
  (define-key global-map "\C-ca" 'org-agenda)
  (setq org-log-done t)
  )

(org-mode-setup)


(defun modify-zap ()
  ;; Replace zap-to-char with zap-up-to-char
  (autoload 'zap-up-to-char "misc"
    "Kill up to, but not including ARGth occurrence of CHAR.
  
   \(fn arg char)"
    'interactive)

  (global-set-key "\M-z" 'zap-up-to-char))

(modify-zap)


(defun virtualenv-setup ()
  ;; Hint: M-x venv-workon and M-x venv-deactivate
  (use-package dash :ensure dash)
  (use-package s :ensure s)
  ;; virtualenvwrapper.el not in melpa
  ;; manually provided in .emacs.d/lisp
  (use-package virtualenvwrapper)
  (venv-initialize-interactive-shells)
  (setq venv-location "/Users/khe/.virtualenvs/"))


(defun fci-setup ()
 ;; Highlight character at "fill-column" position.
 ;; M-x fci-mode
 (use-package fill-column-indicator :ensure fill-column-indicator)

 ;;; Config on graphics displays
 ; (setq fci-rule-width 1)
 ; (setq fci-rule-use-dashes 't)
 ; (setq fci-rule-color "dark-blue")

 ;;; Config on character terminals
 ; (setq fci-rule-character '|)
 ; (setq fci-rule-character-color "dark-blue")

 ;;; Use characters everywhere, even if graphics terminal
 ; (setq fci-always-se-textual-rule 't)

 (fci-mode))

(add-hook 'text-mode-hook 'auto-fill-mode)
(add-hook 'text-mode-hook (lambda () (set-fill-column 78)))
(use-package fill-column-indicator :ensure fill-column-indicator)

;;; OS-X doesn't have ispell
;;; Use ports to install aspell
(setq ispell-program-name "aspell")

(add-hook 'python-mode-hook
	  (lambda ()
	    (turn-on-auto-fill)
	    (fci-setup)
	    ;;(defvar use-yasnippet t)
	    ;;(defvar use-autocomplete t)
            ;;(defvar use-pyflakes-pep8 t)
            ;;(defvar use-flymake nil)
            ;;(set-background-color "blue")
            ;;(hl-line-mode)  ; highlight current line, this mode only
	    (virtualenv-setup)

	    ;; PEP8 compliance stuff
	    (setq indent-tabs-mode nil) ;; only use spaces for indentation
	    (setq tab-width 4)
	    (setq python-indent 4)
	    (setq tab-stop-list (number-sequence 4 120 4))))

(use-package markdown-mode
  :config (progn
	    (add-to-list 'auto-mode-alist '("\\.md$"
					    . markdown-mode)))
  :ensure t)

(use-package rst 
  :config (progn
	    (add-to-list 'auto-mode-alist '("\\.txt$"  . rst-mode))
	    (add-to-list 'auto-mode-alist '("\\.rst$"  . rst-mode))
	    (add-to-list 'auto-mode-alist '("\\.rest$" . rst-mode)))
  :ensure t)


;(set-face-attribute 'default nil
;                    :family "Consolas" :height 145 :weight 'normal)
(set-face-attribute 'default nil
                    :family "Monaco" :height 125 :weight 'normal)

(use-package solarized-theme :ensure t)
(load-theme 'solarized-dark t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; TODO -- stuff to consider
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; * emacs-git-gutter  -- shows changes from git in gutter
;;   or git-gutter-frings

;; * rewrite so that use-package calls are like the one for 'rst in that
;;   they contain config and init code,  which will clean things up a
;;   lot

;; * example of clean .emacs
;;   https://github.com/jordonbiondo/.emacs.d/blob/master/init.el

;; * bind apropos to a global key

;; * markdown viewer for markdown-mode
